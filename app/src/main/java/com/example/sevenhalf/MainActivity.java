package com.example.sevenhalf;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    private ImageView logoiv;
    private Button btnOnep;
    private Button btnTwop;

    private int isActivityReopened = 0;
    public static final String PREF_FILE_NAME = "MySharedFile";
    private int storedPlayerScore;
    private int storedPlayer2Score;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        //Hooks
        logoiv = findViewById(R.id.logoiv);
        btnOnep = findViewById(R.id.btn1player);
        btnTwop = findViewById(R.id.btn2player);

        animationStart();
        resetScores();              //Doing this cleans the file with data stored in it, it's intended that closing and then opening the app
                                    //grants you a clean start

        btnOnep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationLeave(true, false);
                setMode(true);        //Remembers last selected mode to know whether show resume game or not
            }
        });

        btnTwop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                animationLeave(false, false);
                setMode(false);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        isActivityReopened++;                   //Skips first, joins second
        if (isActivityReopened > 1) {
            //Hooks
            btnOnep = findViewById(R.id.btn1player);
            btnTwop = findViewById(R.id.btn2player);

            animationOnResume();

            readScores();

            if (isSinglePlayer()) {         //If the last mode selected was single player
                                            //For the last selected mode, shows 2 options onClick, for the other goes into the game straight away
                btnOnep.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        animationOnResumeSwap();                                //The buttons do an animation and change their texts to 'resume game' and 'new game'

                        btnOnep.setOnClickListener(new View.OnClickListener() {     //Resume game
                            @Override
                            public void onClick(View view) {
                                animationLeave(true, true);
                                setMode(true);
                            }
                        });

                        btnTwop.setOnClickListener(new View.OnClickListener() {     //New game
                            @Override
                            public void onClick(View view) {
                                animationLeave(true, false);
                                setMode(false);
                            }
                        });
                    }
                });

                btnTwop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        animationLeave(false, false);
                        setMode(false);
                    }
                });
            } else {
                btnOnep.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        animationLeave(true, false);
                        setMode(true);
                    }
                });

                btnTwop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        animationOnResumeSwap();

                        btnOnep.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                animationLeave(false, true);
                                setMode(true);
                            }
                        });

                        btnTwop.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                animationLeave(false, false);
                                setMode(false);
                            }
                        });
                    }
                });
            }
        }
    }

    private void resetScores() {
        SharedPreferences sharPrefs = getSharedPreferences(MainActivity.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharPrefs.edit();
        editor.putInt("score1", 0);
        editor.putInt("score2", 0);
        editor.commit();
    }

    private void readScores() {
        SharedPreferences sharPrefs = getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);

        storedPlayerScore = sharPrefs.getInt("score1", 0);
        storedPlayer2Score = sharPrefs.getInt("score2", 0);
    }

    private void setMode(boolean mode) {              //Writes the last selected mode
        //Should be done with normal variables and not the file, but there's multiple issues with reloading applications and the state of variables
        SharedPreferences sharPrefs = getSharedPreferences(MainActivity.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharPrefs.edit();
        editor.putBoolean("modeSinglePlayer", mode);
        editor.commit();
    }

    private boolean isSinglePlayer() {
        SharedPreferences sharPrefs = getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);

        return sharPrefs.getBoolean("modeSinglePlayer", true);
    }

    private void animationStart() {
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator rotateAnimator = ObjectAnimator.ofFloat(logoiv, "rotation", 360f, 0f).setDuration(1000);
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(logoiv, "scaleX", 7f).setDuration(1000);
        scaleXAnimator.setStartDelay(1000);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(logoiv, "scaleY", 7f).setDuration(1000);
        scaleYAnimator.setStartDelay(1000);

        //Displaces button out of screen so it could remain alpha = 1, but still...
        ObjectAnimator alpha1 = ObjectAnimator.ofFloat(btnOnep, "alpha", 0f, 1f);
        alpha1.setStartDelay(2000);
        ObjectAnimator alpha2 = ObjectAnimator.ofFloat(btnTwop, "alpha", 0f, 1f);
        alpha2.setStartDelay(2000);
        ObjectAnimator displaceBtn1 = ObjectAnimator.ofFloat(btnOnep, "translationX",  -1000, 0).setDuration(700);  //relative position is 0
        displaceBtn1.setStartDelay(2000);
        ObjectAnimator displaceBtn2 = ObjectAnimator.ofFloat(btnTwop, "translationX", 1000, 0).setDuration(700);
        displaceBtn2.setStartDelay(2000);

        animatorSet.playTogether(rotateAnimator, scaleXAnimator, scaleYAnimator, alpha1, alpha2, displaceBtn1, displaceBtn2);
        animatorSet.start();
    }

    private void animationLeave(boolean singlePlayer, boolean resumeGame) {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator displaceBtn1 = ObjectAnimator.ofFloat(btnOnep, "translationX", -1000).setDuration(700);
        ObjectAnimator displaceBtn2 = ObjectAnimator.ofFloat(btnTwop, "translationX", 1000).setDuration(700);
        animatorSet.playTogether(displaceBtn1, displaceBtn2);
        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {        //So it waits for the animation to end before doing the intent
                super.onAnimationEnd(animation);
                Intent intent = new Intent(MainActivity.this, Player.class);
                intent.putExtra("gamemode", singlePlayer);
                intent.putExtra("score1", 0);
                intent.putExtra("score2", 0);
                if (resumeGame) {
                    intent.putExtra("score1", storedPlayerScore);
                    intent.putExtra("score2", storedPlayer2Score);
                }
                startActivity(intent);
            }
        });
        animatorSet.start();
    }

    private void animationOnResume() {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator displaceBtn1 = ObjectAnimator.ofFloat(btnOnep, "translationX",  -1000, 0).setDuration(700);  //relative position is 0
        ObjectAnimator displaceBtn2 = ObjectAnimator.ofFloat(btnTwop, "translationX", 1000, 0).setDuration(700);
        btnOnep.setText(R.string.btnOne);       //Only necessary for onResume, but doesn't hurt
        btnTwop.setText(R.string.btnTwo);
        animatorSet.playTogether(displaceBtn1, displaceBtn2);
        animatorSet.start();
    }

    private void animationOnResumeSwap() {
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator displaceBtn1 = ObjectAnimator.ofFloat(btnOnep, "translationX", -1000).setDuration(700);
        ObjectAnimator displaceBtn2 = ObjectAnimator.ofFloat(btnTwop, "translationX", 1000).setDuration(700);
        animatorSet.playTogether(displaceBtn1, displaceBtn2);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                animationOnResume();
                btnOnep.setText(R.string.resumeGame);
                btnTwop.setText(R.string.newGame);
            }
        });

        animatorSet.start();
    }
}