package com.example.sevenhalf;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Player extends AppCompatActivity {

    private Button btnPlay;
    private Button btnOneMore;
    private Button btnStop;
    private Button btnOneMore2;
    private Button btnStop2;
    private TextView textViewScore;
    private TextView textVBankLabel;
    private TextView textVPlayerLabel;
    private TextView bankScoreLabel;
    private TextView youScoreLabel;
    private TextView bankScore;
    private TextView bankScoreTotal;
    private TextView youScore;
    private TextView youScoreTotal;
    private TextView txtWon;
    private TextView txtLost;
    private TextView tvBank;
    private ImageView ivBackground;
    private ImageView imageBackMove;

    private List<Card> cards = new ArrayList<>();       //Can't use HashSet sadly, because doesn't own a get method

    private int cardsThrown;
    private int delay;
    private float bankScoreInternal;
    private int playerBank;
    private int currentPlayer;

    private boolean isActivityReopened = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oneplayer);
        getSupportActionBar().hide();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        //Hooks
        btnPlay = findViewById(R.id.btnPlay);
        btnOneMore = findViewById(R.id.btnOneMore);
        btnStop = findViewById(R.id.btnStop);
        btnOneMore2 = findViewById(R.id.btnOneMore2);
        btnStop2 = findViewById(R.id.btnStop2);
        textViewScore = findViewById(R.id.textViewScore);
        textVBankLabel = findViewById(R.id.textVBankLabel);
        textVPlayerLabel = findViewById(R.id.textVPlayerLabel);
        bankScoreLabel = findViewById(R.id.bankScoreLabel);
        youScoreLabel = findViewById(R.id.youScoreLabel);
        bankScore = findViewById(R.id.bankScore);
        bankScoreTotal = findViewById(R.id.textVBank);
        youScore = findViewById(R.id.youScore);
        youScoreTotal = findViewById(R.id.textVPlayer);
        txtWon = findViewById(R.id.txtWon);
        txtLost = findViewById(R.id.txtLost);
        tvBank = findViewById(R.id.tvBank);
        ivBackground = findViewById(R.id.ivBackground);
        imageBackMove = findViewById(R.id.imageBackMove);

        cardsThrown = 1;
        delay = 0;
        bankScoreInternal = 0;
        playerBank = 0;
        currentPlayer = 0;

        //Boolean to know whether it's single player or multiplayer mode
        Intent intent = getIntent();
        boolean gamemode = intent.getExtras().getBoolean("gamemode");

        if (!gamemode) {
            ivBackground.setImageResource(R.drawable.twoplayers);

            youScoreLabel.setText(R.string.player1);
            bankScoreLabel.setText(R.string.player2);
            textVPlayerLabel.setText(R.string.player1);
            textVBankLabel.setText(R.string.player2);

            textViewScore.setTextColor(Color.BLACK);
            textVBankLabel.setTextColor(Color.BLACK);
            textVPlayerLabel.setTextColor(Color.BLACK);
            youScoreLabel.setTextColor(Color.BLACK);
        }

        animationPlayButtonAppear(true, false);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initializeCards();
                animationPlayButtonDisappear();

                reset();
                txtWon.setAlpha(0);
                txtLost.setAlpha(0);

                int timer = 0;

                if (!gamemode) {        //If it's NOT single player
                    timer = 3000;
                    playerBank = animationBankMove();
                    if (playerBank == 1) {      //In the case player 1 is the bank, cardsThrown is 15, meaning that player 2 starts drawing
                        cardsThrown = 15;
                    }
                }
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        cardsThrown = operateCards(cardsThrown, true);
                    }
                };
                Handler h = new Handler(Looper.getMainLooper());
                // The Runnable will be executed after the given delay time
                h.postDelayed(r, timer); //Will be delayed for 3 seconds
            }
        });

        btnOneMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentPlayer == 0 || currentPlayer == 1) {                           //Knows which buttons to hide (the ones on display)
                    animationButtonsSlideAway(btnOneMore, btnStop);
                } else {
                    animationButtonsSlideAway(btnOneMore2, btnStop2);
                }

                cardsThrown = operateCards(cardsThrown, true);
            }
        });

        btnOneMore2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnOneMore.performClick();              //If you click btnOneMore2, does the same as if you pressed btnOneMore, it's a visual matter
            }
        });

        if (gamemode) {                                                 //How the app behaves if it's single player mode
            btnStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    animationButtonsSlideAway(btnOneMore, btnStop);

                    //Bank's turn
                    cardsThrown = 15;

                    //BankScoreInternal holds the accumulated value of the bankScore. Due to the while loop being too fast to wait for the animations to end,
                    //there needs to be a variable that counts for that, instead of getting the value from the textView. And that variable can be taken advantage of later, see operateCards
                    //so you can have a temporal variable of a given moment that won't be refreshed faster than the animation, this way
                    //making it possible to refresh the score of the bank as it increases :)
                    while (checkEndV2(bankScoreInternal) && bankScoreInternal < Float.parseFloat(youScore.getText().toString()) && cardsThrown <= 24) {        //Draws cards as long as the number is lower than 7 and lower than the player's score
                        cardsThrown = operateCards(cardsThrown, false);
                    }

                    //If the bank has a score lower than 7 AND bigger or equal to yours, you lost, otherwise you win
                    if (bankScoreInternal <= 7 && bankScoreInternal >= Float.parseFloat(youScore.getText().toString())) {
                        animationWinLose(txtLost);
                        animationAddPoints(bankScoreTotal, true);
                    } else {
                        animationWinLose(txtWon);
                        animationAddPoints(youScoreTotal, true);
                    }

                    btnPlay.setText(getResources().getString(R.string.playagain));
                    animationPlayButtonAppear(false, true);

                    bankScoreInternal = 0;
                    delay = 0;
                }
            });
        } else {                                                        //In case it's multiplayer mode
            btnStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currentPlayer == 1) {                           //Knows which buttons to hide (the ones on display)
                        animationButtonsSlideAway(btnOneMore, btnStop);
                    } else {
                        animationButtonsSlideAway(btnOneMore2, btnStop2);
                    }

                    //If player 1 is the bank, after pressing stop I will check if the last card drawn was bigger than 14
                    //The game works in the way that bank plays second, so if player1 is bank, he plays second, therefore after
                    //pressing stop (player 2 stops) it should be his turn, EXCEPT if he's the one to press it (Meaning this is the second
                    //time in the turn that stop is pressed), in which case we will check who won.
                    if (playerBank == 1 && cardsThrown > 14) {
                        cardsThrown = 1;        //Now player1's turn begins
                        currentPlayer = 1;
                    } else if (playerBank == 2 && cardsThrown < 15) {       //Same logic for player2 being the bank
                        cardsThrown = 15;       //Now player2's turn begins
                        currentPlayer = 2;
                    } else {                    //In case one of the clauses broke, meaning that it's the bank who pressed stop
                        float player1score = Float.parseFloat(youScore.getText().toString());
                        float player2score = Float.parseFloat(bankScore.getText().toString());
                        if (playerBank == 1) {
                            if (player1score >= player2score) {
                                txtLost.setText(R.string.player1wins);
                                animationAddPoints(youScoreTotal, false);
                            } else {
                                txtLost.setText(R.string.player2wins);
                                animationAddPoints(bankScoreTotal, false);
                            }
                        } else {
                            if (player2score >= player1score) {
                                txtLost.setText(R.string.player2wins);
                                animationAddPoints(bankScoreTotal, false);
                            } else {
                                txtLost.setText(R.string.player1wins);
                                animationAddPoints(youScoreTotal, false);
                            }
                        }
                        currentPlayer = 0;
                        animationLose();
                        btnPlay.setText(getResources().getString(R.string.playagain));
                        animationPlayButtonAppear(false, false);
                    }
                    if (currentPlayer != 0) {
                        if (currentPlayer == 1) {
                            animationButtonsSlideFirstTime(btnOneMore, btnStop);
                        } else {
                            animationButtonsSlideFirstTime(btnOneMore2, btnStop2);
                        }
                    }
                }
            });
        }

        btnStop2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStop.performClick();              //If you click btnOneMore2, does the same as if you pressed btnOneMore, it's a visual matter
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        Intent intent = getIntent();
        youScoreTotal.setText(String.valueOf(intent.getExtras().getInt("score1")));
        bankScoreTotal.setText(String.valueOf(intent.getExtras().getInt("score2")));
    }

    private void initializeCards() {        //Clears the ArrayList cards and adds new random cards to it.
        cards.clear();
        for (int i = 1; i <= 12; i++) {         //Adds cards to an ArrayList, they are obtained by its name and stored
            String cardName = formatNumbers(i);
            float calcdValue = calculateValue(i);
            int cardId = getResources().getIdentifier("clubs" + cardName, "drawable", getPackageName());
            cards.add(new Card(cardId, calcdValue));
            cardId = getResources().getIdentifier("cups" + cardName, "drawable", getPackageName());
            cards.add(new Card(cardId, calcdValue));
            cardId = getResources().getIdentifier("golds" + cardName, "drawable", getPackageName());
            cards.add(new Card(cardId, calcdValue));
            cardId = getResources().getIdentifier("swords" + cardName, "drawable", getPackageName());
            cards.add(new Card(cardId, calcdValue));
        }           //Could be prettier
        Collections.shuffle(cards);     //Randomizes the cards in the ArrayList
    }

    private void reset() {              //Resets the UI so cards stop showing, used after pressing play again
        youScore.setText("0");
        bankScore.setText("0");
        for (int i = 1; i <= 28; i++) {
            ImageView cardIV = findViewById(getResources().getIdentifier("card" + i, "id", getPackageName()));
            if (cardIV.getAlpha() == 1) {
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.play(ObjectAnimator.ofFloat(cardIV, "alpha", 1f, 0f).setDuration(0));
                animatorSet.start();
            }
        }
        if (playerBank != 0) {          //If it's multiplayer also resets the Bank word position
            tvBank.setY(imageBackMove.getY() + (imageBackMove.getHeight() / 2));
        }

        cardsThrown = 1;
    }

    //Given an int, if the given number is lower or equal to 7, returns a float of that number, else returns 0.5, serves the purpose of counting
    //of how much value is a card. 5 is worth 5, while 11 is worth 0.5
    private float calculateValue(int number) { return number <= 7 ? number : 0.5f; }

    private Card getACard() {           //Draws a card from the ArrayList cards, removes it from said ArrayList, and returns it
        Card card = cards.get(0);
        cards.remove(card);
        return card;
    }

    private int operateCards(int cardsThrown, boolean player) {             //Main method, called to draw each card, player's or bank's
        AnimatorSet animatorSet = new AnimatorSet();

        //Get current card
        ImageView cardIV = findViewById(getResources().getIdentifier("card" + cardsThrown, "id", getPackageName()));

        //Animates imageBackMove to the target position
        ObjectAnimator translationXAnimator = ObjectAnimator.ofFloat(imageBackMove, "translationX", cardIV.getX() - imageBackMove.getLeft()).setDuration(1000);
        ObjectAnimator translationYAnimator = ObjectAnimator.ofFloat(imageBackMove, "translationY", cardIV.getY() - imageBackMove.getTop()).setDuration(1000);
        ObjectAnimator rotationAnimator = ObjectAnimator.ofFloat(imageBackMove, "rotationY", 0f, 90f).setDuration(250);
        rotationAnimator.setStartDelay(1000);

        //imageBackMove now comes back to the origin
        ObjectAnimator comeBackXAnimator = ObjectAnimator.ofFloat(imageBackMove, "translationX", 0f).setDuration(0);
        comeBackXAnimator.setStartDelay(1250);
        ObjectAnimator comeBackYAnimator = ObjectAnimator.ofFloat(imageBackMove, "translationY", 0f).setDuration(0);
        comeBackYAnimator.setStartDelay(1250);
        ObjectAnimator regenerateRotationAnimator = ObjectAnimator.ofFloat(imageBackMove, "rotationY",270f, 0f).setDuration(0);
        regenerateRotationAnimator.setStartDelay(1250);

        //Gets a random card
        Card newCard = getACard();

        //Card n shows up with a rotation of 90°
        ObjectAnimator showUpAnimator = ObjectAnimator.ofFloat(cardIV, "alpha", 0f, 1f).setDuration(0);
        showUpAnimator.setStartDelay(1250);
        ObjectAnimator rotateAnimatorCard = ObjectAnimator.ofFloat(cardIV, "rotationY", 270f, 360f).setDuration(250);
        rotateAnimatorCard.setStartDelay(1250);

        //Set the resource to that card, so it shows the proper image
        cardIV.setImageResource(newCard.getImageCard());

        animatorSet.playTogether(translationXAnimator, translationYAnimator, rotationAnimator, comeBackXAnimator, comeBackYAnimator, regenerateRotationAnimator, showUpAnimator, rotateAnimatorCard);

        if (player) {               //In case it's player's turn
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {        //So it waits for the animation to end before starting the other animation
                    super.onAnimationEnd(animation);
                    if (cardsThrown == 1) {             //Player 1, or Single Player                           //Can never lose in turn 1
                        youScore.setText(String.valueOf(Float.parseFloat(youScore.getText().toString()) + newCard.getValue()));
                        animationButtonsSlideFirstTime(btnOneMore, btnStop);
                    } else if (cardsThrown == 15) {     //Player 2 in multiplayer
                        bankScore.setText(String.valueOf(Float.parseFloat(bankScore.getText().toString()) + newCard.getValue()));
                        animationButtonsSlideFirstTime(btnOneMore2, btnStop2);
                    } else {
                        if (cardsThrown < 15) {                         //Player 1
                            youScore.setText(String.valueOf(Float.parseFloat(youScore.getText().toString()) + newCard.getValue()));
                            resolve(youScore.getText().toString());
                        } else {                                        //Player 2
                            bankScore.setText(String.valueOf(Float.parseFloat(bankScore.getText().toString()) + newCard.getValue()));
                            resolve(bankScore.getText().toString());
                        }
                    }
                }
            });
        } else {                    //In case it's bank's turn
            animatorSet.setStartDelay(delay);
            delay += 2000;
            //BankScoreInternal goes at the speed of the while, who doesn't ever wait for an animation to end before continuing to the next loop
            //It's used to calculate the accumulated value of the bank's score
            bankScoreInternal += newCard.getValue();
            //BankScoreInternalPresent holds the current value of bankScoreInternal, this way being possible to use it after the animation ends
            //If you didn't do that, the animation would end and grab bankScoreInternal, which would be way ahead, thus giving a value that's not
            //the current of the card, but the final one.
            //Being that the first card is a 2 and the second a 4, bsi would show you 6 from the beginning, and bsip would show 2, and later 6
            float bankScoreInternalPresent = bankScoreInternal;
            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    bankScore.setText(String.valueOf(bankScoreInternalPresent));        //When the animation has ended, adds the current and proper value
                }
            });
        }

        animatorSet.start();

        return cardsThrown + 1;
    }

    private void resolve(String score) {                                 //Checks if the player already lost or its count is still below 7
        if (checkEnd(score)) {              //Game still running
            if (currentPlayer == 0 || currentPlayer == 1) {              //Either single player or player 1's turn
                animationButtonsSlide(btnOneMore, btnStop);
            } else {                                                     //Player 2's turn in multiplayer
                animationButtonsSlide(btnOneMore2, btnStop2);
            }
        } else {                            //Game over
            boolean player1won = false;
            if (playerBank != 0) {          //Multiplayer
                //Only gets here if someone surpassed 7, so if it's not player 1, it is player 2
                player1won = checkEnd(youScore.getText().toString());    //Tells me whether player 1 won (true) or player 2 did (false)
                if (player1won) {                                        //If player 1 won and it's multiplayer
                    txtLost.setText(R.string.player1wins);               //Edits the losing animation text so it says "PLAYER 1 WON!"
                } else {
                    txtLost.setText(R.string.player2wins);               //Edits the losing animation text so it says "PLAYER 2 WON!"
                }
            }
            animationLose();
            if (player1won) {
                animationAddPoints(youScoreTotal, false);       //If player 1 won, adds +1 to their score
            } else {
                animationAddPoints(bankScoreTotal, false);      //If it's single player or player 2 won, adds +1 to the bank (player's 2 score)
            }
            //Play button reappears
            btnPlay.setText(getResources().getString(R.string.playagain));
            animationPlayButtonAppear(false, false);
        }
    }

    private void saveScores() {
        SharedPreferences sharPrefs = getSharedPreferences(MainActivity.PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharPrefs.edit();
        editor.putInt("score1", Integer.parseInt(youScoreTotal.getText().toString()));
        editor.putInt("score2", Integer.parseInt(bankScoreTotal.getText().toString()));
        editor.commit();
    }

    private void animationAddPoints(View result, boolean isDelayed) {       //Adds points to either the player or the bank
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(result, "scaleX", result.getScaleX() * 5).setDuration(500);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(result, "scaleY", result.getScaleY() * 5).setDuration(500);

        animatorSet.playTogether(scaleXAnimator, scaleYAnimator);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                //Change to new value
                TextView textView = (TextView) result;
                textView.setText(String.valueOf(Integer.parseInt(textView.getText().toString()) + 1));

                //Animate back in
                AnimatorSet animatorSet2 = new AnimatorSet();

                ObjectAnimator scaleXAnimator2 = ObjectAnimator.ofFloat(result, "scaleX", result.getScaleX() / 5).setDuration(500);
                ObjectAnimator scaleYAnimator2 = ObjectAnimator.ofFloat(result, "scaleY", result.getScaleY() / 5).setDuration(500);

                animatorSet2.playTogether(scaleXAnimator2, scaleYAnimator2);
                animatorSet2.start();
            }
        });

        if (isDelayed) {
            animatorSet.setStartDelay(delay);
        }

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                saveScores();                                   //Saves the scores after they're updated in the UI
            }
        });

        animatorSet.start();
    }

    private void animationLose() {                      //The text for a loss shows up, method ONLY while player is playing, not bank
                                                        //A player can never win before the bank has played, so it's not necessary to add a win text
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(txtLost, "alpha", 0f, 1f).setDuration(0);
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(txtLost, "scaleX", txtLost.getScaleX() * 3, txtLost.getScaleX()).setDuration(1000);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(txtLost, "scaleY", txtLost.getScaleY() * 3, txtLost.getScaleY()).setDuration(1000);

        animatorSet.playTogether(alphaAnimator, scaleXAnimator, scaleYAnimator);

        animatorSet.start();
    }

    private void animationWinLose(View result) {        //The text for a win or a loss shows up, method for the bank, accepts win or loss and
                                                        //applies a delay for it to start after all the card animations have been completed
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(result, "alpha", 0f, 1f).setDuration(0);
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(result, "scaleX", result.getScaleX() * 3, result.getScaleX()).setDuration(1000);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(result, "scaleY", result.getScaleY() * 3, result.getScaleY()).setDuration(1000);

        animatorSet.playTogether(alphaAnimator, scaleXAnimator, scaleYAnimator);

        animatorSet.setStartDelay(delay);

        animatorSet.start();
    }

    private void animationPlayButtonAppear(boolean turn1, boolean isDelayed) {      //Play button shows up
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator scaleXAnimator;
        ObjectAnimator scaleYAnimator;
        if (turn1) {        //The first turn it can just go from n/100 to n
            scaleXAnimator = ObjectAnimator.ofFloat(btnPlay, "scaleX", btnPlay.getScaleX() / 100, btnPlay.getScaleX()).setDuration(1000);
            scaleYAnimator = ObjectAnimator.ofFloat(btnPlay, "scaleY", btnPlay.getScaleY() / 100, btnPlay.getScaleY()).setDuration(1000);
        } else {            //But from there, once animationPlayButtonDisappear has been pressed at least once,
                            //the size of the button Play is already n/100, so it needs to increase rather than start from a small copy of itself
            scaleXAnimator = ObjectAnimator.ofFloat(btnPlay, "scaleX", btnPlay.getScaleX(), btnPlay.getScaleX() * 100).setDuration(1000);
            scaleYAnimator = ObjectAnimator.ofFloat(btnPlay, "scaleY", btnPlay.getScaleY(), btnPlay.getScaleY() * 100).setDuration(1000);
        }
        scaleXAnimator.setStartDelay(100);
        scaleYAnimator.setStartDelay(100);
        animatorSet.playTogether(scaleXAnimator, scaleYAnimator);

        if (isDelayed) {
            animatorSet.setStartDelay(delay);
        }

        animatorSet.start();

        btnPlay.setClickable(true);
    }

    private void animationPlayButtonDisappear() {       //Play button animation, becomes small and not clickable
        AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(btnPlay, "scaleX", btnPlay.getScaleX(), btnPlay.getScaleX() / 100).setDuration(1000);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(btnPlay, "scaleY", btnPlay.getScaleY(), btnPlay.getScaleY() / 100).setDuration(1000);

        animatorSet.playTogether(scaleXAnimator, scaleYAnimator);
        animatorSet.start();

        btnPlay.setClickable(false);
    }

    private void animationButtonsSlideFirstTime(View btnOneMorePlayer, View btnStopPlayer) {     //Buttons of one more and stop come into the screen for the first time
                                                        //Uses animationButtonsSlide so it shows in, and also grants it an alpha = 1
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                ObjectAnimator.ofFloat(btnOneMorePlayer, "alpha", 0f, 1f).setDuration(0),
                ObjectAnimator.ofFloat(btnStopPlayer, "alpha", 0f, 1f).setDuration(0)
        );
        animatorSet.start();
        animationButtonsSlide(btnOneMorePlayer, btnStopPlayer);
    }

    private void animationButtonsSlide(View btnOneMorePlayer, View btnStopPlayer) {              //Buttons of one more and stop come into the screen
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                ObjectAnimator.ofFloat(btnOneMorePlayer, "translationX", -500, 0).setDuration(400),
                ObjectAnimator.ofFloat(btnStopPlayer, "translationX", 500, 0).setDuration(400)
        );
        animatorSet.start();
        btnOneMorePlayer.setClickable(true);
        btnStopPlayer.setClickable(true);
    }

    private void animationButtonsSlideAway(View btnOneMorePlayer, View btnStopPlayer) {          //Buttons of one more and stop leave the screen
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(
                ObjectAnimator.ofFloat(btnOneMorePlayer, "translationX", -500).setDuration(400),
                ObjectAnimator.ofFloat(btnStopPlayer, "translationX", 500).setDuration(400)
        );
        animatorSet.start();
        btnOneMorePlayer.setClickable(false);
        btnStopPlayer.setClickable(false);
    }

    private int animationBankMove() {
        int playerBankIdentification = (int) (Math.random() * 2 + 1);   //Random number that's either 1 or 2

        AnimatorSet animatorSet = new AnimatorSet();

        //Alpha goes back and forth --> + - + - + move
        ObjectAnimator alphaUp = ObjectAnimator.ofFloat(tvBank, "alpha", 0f, 1f).setDuration(400);
        ObjectAnimator alphaDown = ObjectAnimator.ofFloat(tvBank, "alpha", 1f, 0f).setDuration(400);
        alphaDown.setStartDelay(400);
        ObjectAnimator alphaUp2 = ObjectAnimator.ofFloat(tvBank, "alpha", 0f, 1f).setDuration(400);
        alphaUp2.setStartDelay(800);
        ObjectAnimator alphaDown2 = ObjectAnimator.ofFloat(tvBank, "alpha", 1f, 0f).setDuration(400);
        alphaDown2.setStartDelay(1200);
        ObjectAnimator alphaUp3 = ObjectAnimator.ofFloat(tvBank, "alpha", 0f, 1f).setDuration(400);
        alphaUp3.setStartDelay(1600);
        ObjectAnimator alphaDisplace;
        if (playerBankIdentification == 1) {
            alphaDisplace = ObjectAnimator.ofFloat(tvBank, "translationY", youScoreLabel.getY() - tvBank.getTop()).setDuration(1000);
            currentPlayer = 2;      //Player 1 has just become the bank, so it must be player 2's turn
        } else {
            alphaDisplace = ObjectAnimator.ofFloat(tvBank, "translationY", bankScoreLabel.getY() - tvBank.getTop()).setDuration(1000);
            currentPlayer = 1;      //Player 2 has just become the bank, so it must be player 1's turn
        }
        alphaDisplace.setStartDelay(2000);

        animatorSet.playTogether(alphaUp, alphaDown, alphaUp2, alphaDown2, alphaUp3, alphaDisplace);    //This doesn't look good at all, but playsequentally just doesn't work with the same animations
        //Total duration 3000ms
        animatorSet.start();

        return playerBankIdentification;    //Returns eiter 1 or 2 so I know who is the bank
    }

    private boolean checkEnd(String score) {            //Returns true if the given score is lesser or equal to 7, else returns false
        float num = Float.parseFloat(score);
        return num <= 7;
    }

    private boolean checkEndV2(float num) {             //Same as checkEnd but oriented to a float, useful for bankScores, doesn't include equal to 7 as a true statement
        return num < 7;
    }

    private String formatNumbers(int num) {             //Formats a number so if you give it the parameter 4, returns 04
        if (num < 10) return "0" + num;
        return String.valueOf(num);
    }
}