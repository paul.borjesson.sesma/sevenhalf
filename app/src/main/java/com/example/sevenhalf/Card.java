package com.example.sevenhalf;

import java.util.Objects;

public class Card {

    int imageCard;
    float value;

    public Card(int imageCard, float value) {
        this.imageCard = imageCard;
        this.value = value;
    }

    public int getImageCard() {
        return imageCard;
    }

    public float getValue() {
        return value;
    }

    @Override                               //Shouldn't be necessary, but still, it makes sure there's no repeated cards (checks the drawable id)
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return imageCard == card.imageCard;
    }

    @Override
    public int hashCode() {
        return Objects.hash(imageCard);
    }
}
